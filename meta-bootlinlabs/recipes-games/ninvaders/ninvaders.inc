DESCRIPTION="nInvaders console invaders game"
HOMEPAGE="http://ninvaders.sourceforge.net/"
PRIORITY="optional"
SECTION="games"
LICENSE="GPLv2"
LIC_FILES_CHKSUM="file://gpl.txt;md5=393a5ca445f6965873eca0259a17f833"

DEPENDS="ncurses"

EXTRA_OEMAKE = "-e"
do_compile() {
    oe_runmake
}

PACKAGE_BEFORE_PN += "${PN}-data"

do_install() {
	install -d ${D}${bindir}
	install -m 755 nInvaders ${D}${bindir}/ninvaders
	install -d ${D}/data
	install -m 755 nInvaders ${D}/data/ninvaders-data
}

FILES_${PN} += "/data/*"
FILES_${PN}-data = "/data/*"
